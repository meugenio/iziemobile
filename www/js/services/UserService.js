services.factory('User', function(http_izie) {

  return {
    all: function() {
      return http_izie.get('user-all');
    },

    create: function(params) {
      return http_izie.post('user-create', params);
    },

    update: function (params) {
      return http_izie.put('user-update', params);
    },

    remove: function(id) {
      return http_izie.delete('user-delete', { params: { id: id } });
    },

    get: function(id) {
      return http_izie.get('user-show', { params: { id: id } });
    }
  };
});
