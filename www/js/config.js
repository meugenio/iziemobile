var config = angular.module('config-izie',[])


  .constant('apiUrl', 'http://192.168.10.19:8000')

  .constant('api', [
    { api: "user-all", url: "/api/v1/user" },
    { api: "user-show", url: "/api/v1/user/{id}" },
    { api: "user-create", url: "/api/v1/user" },
    { api: "user-update", url: "/api/v1/user" },
    { api: "user-delete", url: "/api/v1/user/{id}" }
  ])


  .factory('API_URL', function(apiUrl, api) {
    return {
      get: function (name) {
        var filter = api.filter( function (value) { return (value.api === name); } );
        if(!filter.length) {
          return apiUrl;
        }

        return apiUrl + filter[0].url;
      }
    }
  });


config.config(function ($httpProvider) {
  $httpProvider.interceptors.push(function ($q) {
    return {
      'request': function (config) {

        var getKeys = function(obj){
          var keys = [];
          for(var key in obj){
            keys.push(key);
          }
          return keys;
        }

        var keys = getKeys(config.params);

        keys.forEach(function(key) {
          config.url = config.url.replace('{' + key + '}', config.params[key]);
        });

        console.log(config.url);

        return config || $q.when(config);
      }

    }
  });
});
