controllers.controller('UserListCtrl', function($scope, User, $state, util_izie, $ionicPopup) {
  $scope.listUser = [];
  $scope.search;

  $scope.$on('$ionicView.enter', function(){
    $scope.initial();
  });

  $scope.initial = function () {
    $scope.listUser = [];

    User.all().then(function (data) {
      $scope.listUser = data;
    });
  }

  $scope.onDelete = function(id) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirmação',
      template: 'Deseja excluir esse registro?'
    });

    confirmPopup.then(function(res) {
      if (res) {
        User.remove(id).then(function (response) {
          util_izie.toast(response.response);
          $scope.initial();
        }).catch(function (data) {
          util_izie.toast(data.error);
        });
      }
    });
  }

  $scope.onCreate = function () {
    $state.go('create');
  }

  $scope.onHome = function () {
    $state.go('main');
  }
});
