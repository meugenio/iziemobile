controllers.controller('UserCreateCtrl', function($scope, User, $state, util_izie, $cordovaCamera, $location, $stateParams, $ionicModal) {
  $scope.image = "";
  $scope.form = {};
  $scope.listAddress = [];
  $scope.address = {};
  $scope.modal;

  $scope.initial = function() {
    var id = $stateParams.id;

    if(id) {
      $scope.loadUser(id);
    } else {
      $scope.form = {};
    }
  }

  $scope.getImage = function() {
    return ($scope.image || $scope.form.imagem);
  }

  $scope.loadUser = function(id) {
    User.get(id).then(function(response) {
      $scope.form = (response);
      $scope.form.nascimento = new Date(response.nascimento);

        $scope.listAddress = response.addresses;
    });
  }

  $scope._validForm = function(form) {
    if(!form.id) {
      if(!$scope.image) {
        util_izie.toast('Tirar foto é obrigatório');
        return false;
      }
    }

    if(!form.nome) {
      util_izie.toast('Nome é obrigatório');
      return false;
    }

    if(!form.nascimento) {
      util_izie.toast('Data de nascimento é obrigatório');
      return false;
    }

    if(!form.sexo) {
      util_izie.toast('Sexo é obrigatório');
      return false;
    }

    if(!form.cpf) {
        util_izie.toast('CPF é obrigatório');
        return false;
    }

    return true;
  }

  $scope.onSave = function () {
    var form = angular.copy($scope.form);
    if($scope.image) {
      form.imagem = $scope.image;
    }

    if(!$scope._validForm(form)) {
      return;
    }

    form.address = $scope.listAddress;

    if(form.id) {
      $scope.onUpdate(form);
      return;
    }

    User.create(form).then(function() {
      $location.path('/user');
    }).catch(function(data) {
      util_izie.toast(data.error);
    });
  }

  $scope.onUpdate = function(form) {
    User.update(form).then(function() {
      $location.path('/user');
    }).catch(function(data) {
      util_izie.toast(data.error);
    });
  }

  $scope.onCamera = function () {
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 200,
      targetHeight: 200,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.image = "data:image/jpeg;base64," + imageData;
    }, function(err) {
      // error
    });
  }

  $scope._validFormAddress = function (addr) {
      if(!addr.cep) {
          util_izie.toast('CEP é obrigatório');
          return false;
      }

      if(!addr.endereco) {
          util_izie.toast('Endereço é obrigatório');
          return false;
      }

      if(!addr.complemento) {
          util_izie.toast('Complemento é obrigatório');
          return false;
      }

      if(!addr.numero) {
          util_izie.toast('Número é obrigatório');
          return false;
      }

      if(!addr.estado) {
          util_izie.toast('Estado é obrigatório');
          return false;
      }

      if(!addr.municipio) {
          util_izie.toast('Município é obrigatório');
          return false;
      }

      return true;
  }

  $scope.addAddress = function() {
      $ionicModal.fromTemplateUrl('./templates/user/address.html', function(modal) {
          $scope.modal = modal;

          $scope.modal.show();
      }, {
          scope: $scope,
          animation: 'slide-in-up'
      });
  }

  $scope.onSaveAddress = function() {
      var form = angular.copy($scope.address);

      if(!$scope._validFormAddress(form)) {
          return;
      }

      $scope.listAddress.push(form);

      $scope.address = {};
      $scope.modal.remove();
  }

  $scope.onDelete = function (index) {
      $scope.listAddress.splice(index, 1);
  }

  $scope.onCancelar = function () {
      $scope.modal.hide();
  }

});
