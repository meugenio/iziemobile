angular.module('factories-izie',[])

.factory('http_izie', function($http, API_URL, $q, $ionicLoading) {

  return {
    get: function(api, params) {
      var q = $q.defer();

      $ionicLoading.show({ template: 'Carregando ...' });

      $http.get(API_URL.get(api), params)
        .success(function(data) {
          q.resolve(data);
          $ionicLoading.hide();
        })
        .error(function(error) {
          q.reject(error);
          $ionicLoading.hide();
        });

      return q.promise;
    },

    post: function(api, params) {
      var q = $q.defer();

      $ionicLoading.show({ template: 'Aguarde ...' });

      $http.post(API_URL.get(api), params)
        .success(function(data) {
          q.resolve(data);
          $ionicLoading.hide();
        })
        .error(function(error) {
          q.reject(error);
          $ionicLoading.hide();
        });

      return q.promise;
    },

    put: function(api, params) {
      var q = $q.defer();

      $ionicLoading.show({ template: 'Aguarde ...' });

      $http.put(API_URL.get(api), params)
        .success(function(data) {
          q.resolve(data);
          $ionicLoading.hide();
        })
        .error(function(error) {
          q.reject(error);
          $ionicLoading.hide();
        });

      return q.promise;
    },

    delete: function(api, params) {
      var q = $q.defer();

      $ionicLoading.show({ template: 'Aguarde ...' });

      $http.delete(API_URL.get(api), params)
        .success(function(data) {
          q.resolve(data);
          $ionicLoading.hide();
        })
        .error(function(error) {
          q.reject(error);
          $ionicLoading.hide();
        });

      return q.promise;
    }
  };
})

.factory('util_izie', function(ionicToast) {
  return {
    toast: function(text) {
      ionicToast.show(text, 'top', false, 5000);
    }
  }
});
